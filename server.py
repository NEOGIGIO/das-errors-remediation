# ----------------------------------
# -----
# To install Flask
# pip install -U Flask
# -----
# To install Pandas
# pip install pandas
# -----
# To install MSExcel support in Pandas
# pip install xlrd
# -----
# To able export to an MSExcel file
# pip install openpyxl
# -----
# To run flask server
# env FLASK_APP=server.py FLASK_ENV=development flask run
# -----
# ----------------------------------

# Our libraries and dependencies
import pandas as pd
from flask import Flask
from flask import request

# Opening Excel Sheets
extras = pd.read_excel (io='./SampleData.xlsx', sheet_name='RegionExtras') 
orders = pd.read_excel (io='./SampleData.xlsx', sheet_name='SalesOrders') 
messages = pd.read_excel (io='./SampleData.xlsx', sheet_name='Messages') 
output = None

# Creating Flask Back-End
app = Flask(__name__)

@app.route('/')
def main_route():
    print('Hello World')
    return 'Hello World'

@app.route('/orders')
def orders_route():
    print(orders)
    return orders.to_json()

@app.route('/extras')
def extras_route():
    print(extras)
    return extras.to_json()

@app.route('/messages')
def messages_route():
    print(messages)
    return messages.to_json()

@app.route('/join')
def join_route():
    global output
    output = pd.merge(extras, orders, how='inner', on=['Region'])
    print(output)
    return output.to_json()

@app.route('/replace')
def replace_route():
    global output
    output = pd.merge(orders, messages, how='inner', on=['Region'])

    for row_number, row in output.iterrows():
        message = output['Message'][row_number]
        qty_to_replace = message.count('&')
        for i in range(qty_to_replace):
            column = output.columns.get_loc('First &') + i
            message = message.replace('&', str(output.iloc[row_number, column]), 1) 
        output.set_value(row_number, 'Message', message)

    print(output)
    return output.to_json()

# To send parameters use this structure
# /add_comment?comment=your_comment&row=your_row_number
@app.route('/add_comment')
def add_comment_route():
    global output
    comment = request.args.get('comment')
    row = request.args.get('row')
    output.set_value(int(row), 'Comments', comment)
    print(output)
    return output.to_json()

@app.route('/save')
def save_route():
    output.to_excel('SampleDataProcessed.xlsx', index=False)
    return 'Saved'

if __name__ == '__main__':
    app.run()